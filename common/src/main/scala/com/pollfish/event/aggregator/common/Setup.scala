package com.pollfish.event.aggregator.common

import java.io.File

import akka.actor.ActorSystem
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.LazyLogging

import scala.io.StdIn

/**
 * A scaffolding app mixin that setups the Main application for both client and server application. It handles configuration discovery,
 * actor system initialization, and proper termination. The actual application logic flow and actor creation must be explicitly implemented.
 */
trait Setup extends LazyLogging {
  this: App =>

  val config: Config = System.getProperty("appConfig", "N/A") match {
    case "N/A" => ConfigFactory.load()
    case configPath: String => ConfigFactory.parseFile(new File(configPath + "application.conf"))
  }
  implicit val system = ActorSystem(actorSystemName, config)
  logger.info("Application is starting...")
  implicit val executionContext = system.dispatcher

  def actorSystemName: String

  /**
   * Start the appropriate actors, read configuration, e.t.c...
   */
  executeApplicationLogic()

  System.getProperty("mode", "N/A") match {
    case "production" => logger.info("Running in production mode")
    case _: Any =>
      logger.info("Press RETURN to stop...")
      StdIn.readLine()
      terminate()
  }

  def executeApplicationLogic()

  protected def terminate() = {
    system.terminate().foreach { _ =>
      logger.info("Actor system was shut down")
    }
  }
}
