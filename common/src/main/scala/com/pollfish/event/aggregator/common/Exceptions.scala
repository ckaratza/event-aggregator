package com.pollfish.event.aggregator.common

/**
 * A runtime exception used to signal connection bind failure. Used by the client application in order to provide the supervisor strategy the chance to apply
 * a restart decision in order to pick the next possible available server.
 */
case class ConnectionBindException() extends RuntimeException

/**
 * A runtime exception used to signal a broken connection. Used by the client application in order to provide the supervisor strategy the chance to apply
 * a restart decision in order to pick the next possible available server.
 */
case class ConnectionClosedException() extends RuntimeException