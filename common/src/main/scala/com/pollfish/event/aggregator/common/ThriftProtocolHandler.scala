package com.pollfish.event.aggregator.common

import org.apache.thrift.{TBase, TDeserializer, TFieldIdEnum, TSerializer}

/**
 * A thrift serializer, deserializer utility that wraps results in an Option.
 */
trait ThriftProtocolHandler {

  private[this] val serializer = new TSerializer()
  private[this] val deSerializer = new TDeserializer()

  /**
   * Attempts to serialize a thrift object type into bytes
   * @param obj
   * @tparam T
   * @return
   */
  def serialize[T <: TBase[_ <: TBase[_, _], _ <: TFieldIdEnum]](obj: T): Option[Array[Byte]] = {
    try {
      Some(serializer.serialize(obj))
    }
    catch {
      case _: Throwable => None
    }
  }

  /**
   * Attempts to deserialize a byte array to a valid thrift object.
   * @param obj
   * @param bytes
   * @tparam T
   * @return
   */
  def deSerialize[T <: TBase[_ <: TBase[_, _], _ <: TFieldIdEnum]](obj: T, bytes: Array[Byte]): Option[T] = {
    try {
      deSerializer.deserialize(obj, bytes)
      Some(obj)
    }
    catch {
      case _: Throwable => None
    }
  }
}
