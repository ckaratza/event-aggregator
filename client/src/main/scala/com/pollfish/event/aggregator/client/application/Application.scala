package com.pollfish.event.aggregator.client.application

import akka.actor._
import com.pollfish.event.aggregator.client.{Client, RoundRobinSelector, Started}
import com.pollfish.event.aggregator.common.{ConnectionBindException, ConnectionClosedException, Setup}
import com.typesafe.config.Config

import scala.collection.JavaConversions._
import scala.concurrent.duration._
import scala.language.postfixOps

/**
 * The Main of the client application.
 */
object Application extends App with Setup {

  override def executeApplicationLogic(): Unit = {
    /**
     * Reads server information from configuration and initializes the round robin selector.
     */
    RoundRobinSelector.init(config.getStringList("client.servers").map(serverInfo => {
      val parts = serverInfo.split(":")
      (parts(0), parts(1).toInt)
    }))

    /**
     * Starts the client supervisor actor, which subsequently starts the client actor.
     */
    system.actorOf(Props(classOf[Supervisor], config), "clientSupervisor")
    setupEventGenerator(config)
  }

  /**
   * Initializes the event generation simulation from configuration.
   *
   * @param config
   * @return
   */
  private[this] def setupEventGenerator(config: Config) = {
    EventGenerator.initialize(config.getInt("simulation.messagesPerSecond"), config.getStringList("simulation.possibleTimeZoneList").toArray[String](Array()),
      config.getStringList("simulation.possibleChannelList").toArray[String](Array()), config.getStringList("simulation.possibleClientList").toArray[String](Array()),
      config.getStringList("simulation.possibleLocationList").toArray[String](Array()), config.getStringList("simulation.possibleEventTypeTemplates").toArray[String](Array()))
  }

  override def actorSystemName: String = "thrift-client"

  override protected def terminate(): Unit = {
    EventGenerator.stop()
    super.terminate()
  }

  /**
   *
   * @param config
   */
  class Supervisor(config: Config) extends Actor {

    import akka.actor.OneForOneStrategy
    import akka.actor.SupervisorStrategy._

    /**
     * The supervisor strategy used for the client. In case of connection exceptions try to restart as many times possible within a range of 10 seconds.
     */
    override val supervisorStrategy =
      OneForOneStrategy(withinTimeRange = 10 seconds) {
        case _: ActorInitializationException => Stop
        case _: ActorKilledException => Stop
        case _: DeathPactException => Stop
        case _: ConnectionBindException => Restart
        case _: ConnectionClosedException => Restart
        case _: Exception => Restart
      }
    /**
     * Instantiate client actor with configuration parameters.
     */
    val client = context.actorOf(Props(classOf[Client], config.getLong("client.backPressure.threshold"),
      config.getInt("client.batchSize"), config.getLong("client.ackTimeout")), "thrift-client")

    def receive = {
      /**
       * Message received from client actor to signal its creation, in order for supervisor to set the correct actorRef to the EventGenerator
       * and possibly resume message submission.
       */
      case Started =>
        context.watch(sender())
        EventGenerator.reset(sender())
        EventGenerator.start()
      /**
       * In case of client's death inform EventGenerator to stop submitting messages.
       */
      case terminated: Terminated => EventGenerator.stop()
    }
  }

}