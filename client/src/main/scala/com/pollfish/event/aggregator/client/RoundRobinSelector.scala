package com.pollfish.event.aggregator.client

import scala.collection.mutable

/**
 * A round robin list selector used from the client application to pick the server location data to connect to. In case of subsequent invocations it uses round robin algorithm to
 * select the next possible available server.
 */
private[client] object RoundRobinSelector {

  /**
   * The list of available server information [(server1,port1), ....].
   */
  private[RoundRobinSelector] var serverList: mutable.Buffer[(String, Int)] = _

  /**
   * Initializes the list of server information.
   *
   * @param serverList
   */
  def init(serverList: mutable.Buffer[(String, Int)]) = this.serverList = serverList

  /**
   * Selects the next possibly available server information applying round robin algorithm.
   * @return
   */
  def getNextServerInfo: (String, Int) = {
    serverList = serverList.tail ++ List(serverList.head)
    serverList.head
  }
}
