package com.pollfish.event.aggregator.client

import java.net.InetSocketAddress
import java.util
import java.util.UUID

import akka.actor._
import akka.io.Tcp.Write
import akka.io.{IO, Tcp}
import akka.util.ByteString
import com.pollfish.event.aggregator.common.{ConnectionBindException, ConnectionClosedException, ThriftProtocolHandler}
import com.pollfish.event.aggregator.model.{Ack, Batch, Event}
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration._
import scala.language.postfixOps

/**
 * A message used internally by the client to signal message dispatching to the server.
 */
private[client] case object Dispatch

/**
 * A message used to signal client's supervisor that it has started.
 */
private[client] case object Started

/**
 * The client actor has the responsibility of receiving messages of type Event, serializing them with the thrift stack, sending them to the server
 * as a Batch of size N and waiting for acknowledgement before proceeding with the next batch. It has an internal buffer to hold messages in case server cannot
 * process at the production rate or it has stopped processing.
 */
private[client] object Client extends ThriftProtocolHandler with LazyLogging {

  var backPressureThreshold: Long = _
  var eventList: java.util.List[Event] = new util.ArrayList[Event]()
  var parcel: Parcel = _
  var batchSize: Int = _
  var outboundConnection: ActorRef = _
  var clientRef: ActorRef = _

  /**
   * Initializes from configuration parameters.
   * @param backPressureThreshold
   * @param batchSize
   */
  def initialize(backPressureThreshold: Long, batchSize: Int) = {
    this.batchSize = batchSize
    this.backPressureThreshold = backPressureThreshold
    parcel = Parcel()
  }

  /**
   * Places received event in internal buffer for further dispatching. If buffer exceeds backPressure threshold, en-queuing fails.
   * @param event
   * @return
   */
  def enqueueEvent(event: Event): Boolean = {
    if (eventList.size() < backPressureThreshold) {
      eventList.add(event)
      logger.debug("EnQueued..")
      true
    }
    else false
  }

  /**
   * Upon Acknowledgement reception from server determine if acknowledgement is valid and corresponds to pending acknowledgement parcel.
   * @param data
   * @return
   */
  def tryToProcessAckMessageAndReturnPendingParcelAck(data: Array[Byte]): (Boolean, Boolean) = {
    val ack = new Ack()
    val oAck = deSerialize[Ack](ack, data)
    logger.debug("Received ack with UUID {}", oAck.get.parcelId)
    if (oAck.nonEmpty) {
      if (parcel.parcelId == oAck.get.parcelId) parcel.reset()
      (true, parcel.parcelId != "")
    }
    else (false, false)
  }

  /**
   * Try to dispatch next batch by taking up to batchSize events and creating a batch, sending it to serer and putting it in a parcel for checking acknowledgement.
   * In case there is a pending parcel, resubmit the batch to server
   */
  def tryToDispatchNextBatches() = {
    if (parcel.parcelId == "" && eventList.size() > 0) {
      val nextBatch = new Batch()
      val parcelId = UUID.randomUUID().toString
      nextBatch.setParcelId(parcelId)
      val eventsToPutInBatch = eventList.subList(0, Math.min(batchSize, eventList.size()))
      for (i <- 0 until eventsToPutInBatch.size()) {
        nextBatch.addToEvents(eventList.get(i))
      }
      val msg = serialize[Batch](nextBatch)
      if (msg.nonEmpty) {
        logger.debug("Sending parcel {}", parcelId)
        outboundConnection ! Write(ByteString.fromArray(msg.get))
        parcel.parcelId = parcelId
        parcel.batch = nextBatch
        eventsToPutInBatch.clear()

      }
    }
    else {
      if (parcel.batch != null) {
        logger.debug("Sending parcel again {}", parcel.batch.getParcelId)
        outboundConnection ! Write(ByteString.fromArray(serialize[Batch](parcel.batch).get))
      }
    }
  }

  /**
   * The parcel used internally to aid acknowledgement mechanism.
   * @param parcelId
   * @param batch
   */
  private[Client] case class Parcel(var parcelId: String = "", var batch: Batch = null) {
    def reset() = {
      parcelId = ""
      batch = null
    }
  }

}

class Client(val backPressureThreshold: Long, val batchSize: Int, val ackTimeout: Long) extends Actor with LazyLogging {

  import Client._
  import RoundRobinSelector._
  import Tcp._
  import context.system

  /**
   * Try to connect to a new server from available server list.
   */
  val serverInfo = getNextServerInfo
  val remote = new InetSocketAddress(serverInfo._1, serverInfo._2)
  logger.info("Attempting to connect to server -> {}", remote)

  IO(Tcp) ! Connect(remote)
  implicit val executor = context.system.dispatcher
  var cancellable: Cancellable = _

  def receive = {
    case CommandFailed(connect: Connect) =>
      logger.info("Failed to obtain connection -> {}", connect.remoteAddress)
      throw new ConnectionBindException()
    case c@Connected(remote, local) =>
      logger.info("Successfully obtained connection -> {}", remote)
      val connection = sender()
      connection ! Register(self)
      outboundConnection = connection
      clientRef = self
      initialize(backPressureThreshold, batchSize)

      /**
       * Schedule dispatching next or pending batch.
       */
      cancellable = context.system.scheduler.scheduleOnce(ackTimeout milliseconds, self, Dispatch)
      context become {
        case Dispatch =>
          tryToDispatchNextBatches()

          /**
           * Reschedule ack timeout.
           */
          cancellable = context.system.scheduler.scheduleOnce(ackTimeout milliseconds, self, Dispatch)
        case data: com.pollfish.event.aggregator.model.Event =>
          if (!enqueueEvent(data)) logger.warn("Could't enqueue event. BackPressure threshold probably exceeded.")
        case CommandFailed(w: Write) =>
          logger.warn("O/S Buffer was Full")
        case Received(data) =>
          logger.debug("Received Possible Ack message.")
          val (properlyDeSerialized, isPending) = tryToProcessAckMessageAndReturnPendingParcelAck(data.toArray)
          if (!properlyDeSerialized) logger.warn("Message not of Ack Type.")
          else if (!isPending) {
            /**
             * cancel ack timeout task and try to dispatch another batch.
             */
            cancellable.cancel()
            self ! Dispatch
          }
        case cc: ConnectionClosed =>
          logger.info("Connection has closed.")
          throw new ConnectionClosedException()
      }
  }

  @throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    super.preStart()
    logger.info("Client has started.")

    /**
     * Signal supervisor that client has started.
     */
    context.parent ! Started
  }

  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    super.postStop()
    logger.info("Client has stopped.")
  }
}