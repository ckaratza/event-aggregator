package com.pollfish.event.aggregator.client.application

import java.util.concurrent.{ScheduledThreadPoolExecutor, TimeUnit}

import akka.actor.ActorRef
import com.pollfish.event.aggregator.model.{Content, Event, Origin, Time}
import com.typesafe.scalalogging.LazyLogging

import scala.language.postfixOps
import scala.util.Random

/**
 * A very simplistic event generator. It generates a specified from configuration number of messages per second by combining random combinations from a configured
 * set of event field data. The event content comes from a small set of fixed messages with specific business types.
 * The generator doesn't contain acknowledgement logic. That means that message submission is fire and forget. So in case of client failure or reconnection
 * with the server some messages will not be processed. The whole system architecture can easily be adjusted to add ack capabilities to event generator and enhancing
 * the client to acknowledge to the generator upon receiving ack from server. For the purpose of the prototype and for personal time constraints I decided not to
 * implement this aspect.
 *
 */
private[application] object EventGenerator extends LazyLogging {

  private[EventGenerator] var ordinal: Long = 0
  private[EventGenerator] var client: ActorRef = _
  private[EventGenerator] var rnd = new Random()
  private[EventGenerator] var messagesPerSecond = 2000
  private[EventGenerator] var possibleTimeZoneList: Array[String] = _
  private[EventGenerator] var possibleChannelList: Array[String] = _
  private[EventGenerator] var possibleClientList: Array[String] = _
  private[EventGenerator] var possibleLocationList: Array[String] = _
  private[EventGenerator] var possibleEventTypeTemplates: Array[String] = _
  private[EventGenerator] var executor: ScheduledThreadPoolExecutor = _
  private[EventGenerator] var future: ScheduledThreadPoolExecutor = _

  def reset(ref: ActorRef) = {
    client = ref
  }

  def initialize(messagesPerSecond: Int, possibleTimeZoneList: Array[String], possibleChannelList: Array[String],
                 possibleClientList: Array[String], possibleLocationList: Array[String], possibleEventTypeTemplates: Array[String]) = {
    this.messagesPerSecond = messagesPerSecond
    this.possibleTimeZoneList = possibleTimeZoneList
    this.possibleChannelList = possibleChannelList
    this.possibleClientList = possibleClientList
    this.possibleLocationList = possibleLocationList
    this.possibleEventTypeTemplates = possibleEventTypeTemplates
  }

  def start() = {
    stop()
    executor = new ScheduledThreadPoolExecutor(1)
    val task = new Runnable {
      def run() = {
        for (i <- 1 to messagesPerSecond) {
          client ! buildEvent()
        }
        logger.info("Total number of messages produced -> {}", ordinal)
      }
    }
    executor.scheduleAtFixedRate(task, 1000, 950, TimeUnit.MILLISECONDS)
  }

  private[this] def buildEvent(): Event = {
    val event = new Event()
    val origin = new Origin()
    origin.setLocation(possibleLocationList(rnd.nextInt(possibleLocationList.length)))
    ordinal = ordinal.+(1)
    event.setOrdinal(ordinal)
    event.setOrigin(origin)
    event.setClientUUID(possibleClientList(rnd.nextInt(possibleClientList.length)))
    event.setChannel(possibleChannelList(rnd.nextInt(possibleChannelList.length)))
    event.setTime(new Time(System.currentTimeMillis(), possibleTimeZoneList(rnd.nextInt(possibleTimeZoneList.length))))
    val (content, eType) = buildRandomJsonContent()
    event.setContent(new Content(content, eType))
    event
  }

  def buildRandomJsonContent(): (String, String) = {
    val templateIndex = rnd.nextInt(possibleEventTypeTemplates.length)
    (possibleEventTypeTemplates(templateIndex), "type" + templateIndex)
  }

  def stop() = {
    logger.info("Stopping simulation...")
    if (executor != null) executor.shutdown()
  }
}