package com.pollfish.event.aggregator.server.application

import java.util.concurrent.{ScheduledThreadPoolExecutor, TimeUnit}

import akka.actor.{ActorRef, Props}
import akka.routing.SmallestMailboxPool
import com.pollfish.event.aggregator.common.Setup
import com.pollfish.event.aggregator.server.{KafkaProducer, Server}
import com.typesafe.scalalogging.LazyLogging

/**
 * The Main of the server application.
 */
object Application extends App with ProgressIndicator with Setup {

  override def executeApplicationLogic(): Unit = {
    val (host, port) = (config.getString("server.host"), config.getInt("server.port"))
    val (topic, brokers) = (config.getString("kafka.targetTopic"), config.getString("kafka.brokers"))
    /**
     * Create the loadBalancer pool-router which deploys a number of kafkaProducer actors and assigns tasks to them by a smallest mailbox strategy.
     * Each kafkaProducer uses a pinned dispatcher.
     */
    val loadBalancerActor: ActorRef = system.actorOf(SmallestMailboxPool(config.getInt("server.kafkaProducersNr")).props(Props(classOf[KafkaProducer], topic, brokers)
      .withDispatcher("pinned-dispatcher")), "loadBalancer-actor")

    /**
     * Create the server actor.
     */
    system.actorOf(Props(classOf[Server], host, port, loadBalancerActor), "thrift-server-actor")
    startProgressLogging()
  }

  override def actorSystemName: String = "thrift-server"

  override protected def terminate(): Unit = {
    super.terminate()
    if (executor != null) executor.shutdown()
  }
}

/**
 * Log number of messages persisted to Kafka.
 */
trait ProgressIndicator {
  this: LazyLogging =>

  val executor = new ScheduledThreadPoolExecutor(1)
  val task = new Runnable {
    def run() = {
      logger.info("Total number of messages persisted to kafka -> {}", KafkaProducer.messagesPersistedToKafka.get())
    }
  }

  def startProgressLogging() = executor.scheduleAtFixedRate(task, 1000, 1000, TimeUnit.MILLISECONDS)
}