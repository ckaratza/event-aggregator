package com.pollfish.event.aggregator.server

import akka.actor.{Actor, ActorRef}
import akka.io.Tcp
import akka.util.ByteString
import com.pollfish.event.aggregator.common.ThriftProtocolHandler
import com.pollfish.event.aggregator.model.Batch
import com.typesafe.scalalogging.LazyLogging

/**
 * The actor that represents the connection with the client. Upon message reception it dispatches to the loadBalancer router after correctly assembling batch message
 * so an instance of the kafkaProducers pool will process the message and commit to kafka.
 *
 * @param loadBalancerActor
 */
class MessageHandler(val loadBalancerActor: ActorRef) extends Actor with LazyLogging with ThriftProtocolHandler {

  import Tcp._

  /**
   * Used for internal buffering to properly deserialize batch object in case (akka) chunks are more than one
   */
  private[this] var byteString = ByteString.empty

  def receive = {
    case Received(data) =>
      logger.debug("Message Received Size {}", data.size)
      val batch = new Batch()
      /**
       * Assemble byte buffer and try to deserialize
       */
      byteString = byteString.++(data)
      val oBatch = deSerialize[Batch](batch, byteString.toByteBuffer.array())
      if (oBatch.nonEmpty && oBatch.get.events != null) {
        /**
         * Send to a KafkaProducer instance through loadBalancer router.
         */
        loadBalancerActor.!(oBatch.get)(sender())
        byteString = ByteString.empty
      }
    case PeerClosed =>
      logger.info("Peer Terminated Connection.")
      context stop self
  }
}


