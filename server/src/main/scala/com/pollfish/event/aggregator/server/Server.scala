package com.pollfish.event.aggregator.server

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorRef, Props}
import akka.io.{IO, Tcp}
import com.typesafe.scalalogging.LazyLogging

/**
 * The server actor which accepts connections from clients and delegates them to new MessageHandlers.
 * @param host
 * @param port
 * @param loadBalancerActor
 */
class Server(host: String, port: Int, loadBalancerActor: ActorRef) extends Actor with LazyLogging {

  import Tcp._
  import context.system

  IO(Tcp) ! Bind(self, new InetSocketAddress(host, port))

  def receive = {
    case b@Bound(localAddress) =>
      logger.info("Server bound -> {}", localAddress)
    case CommandFailed(bind: Bind) =>
      logger.info("Server bind Failure -> {}", bind.localAddress)
      context stop self
    case c@Connected(remote, local) =>
      logger.info("Accepting remote connection -> {}", remote)
      val handler = context.actorOf(Props(classOf[MessageHandler], loadBalancerActor))
      val connection = sender()
      connection ! Register(handler)
  }
}

