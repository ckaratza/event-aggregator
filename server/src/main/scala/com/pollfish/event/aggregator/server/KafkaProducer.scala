package com.pollfish.event.aggregator.server

import java.util.Properties
import java.util.concurrent.atomic.AtomicLong

import akka.actor.Actor
import akka.io.Tcp.Write
import akka.util.ByteString
import com.pollfish.event.aggregator.common.ThriftProtocolHandler
import com.pollfish.event.aggregator.model.{Ack, Batch, Event}
import com.typesafe.scalalogging.LazyLogging
import kafka.javaapi.producer.Producer
import kafka.producer.{KeyedMessage, ProducerConfig}

import scala.collection.JavaConversions._

/**
 * KafkaProducer actor has the responsibility of accepting messages of Batch type, persisting batch event list to kafka and upon completion sending an Ack
 * message back to the client so the flow continues.
 */
object KafkaProducer {
  var messagesPersistedToKafka = new AtomicLong(0)

  def createProducer(brokers: String) = {
    val props = new Properties()
    props.put("metadata.broker.list", brokers)
    props.put("key.serializer.class", "kafka.serializer.StringEncoder")
    props.put("partitioner.class", "com.pollfish.event.aggregator.server.EventPartitioner")
    props.put("producer.type", "sync")
    new Producer[String, Array[Byte]](new ProducerConfig(props))
  }
}

class KafkaProducer(val topic: String, val brokers: String) extends Actor with LazyLogging with ThriftProtocolHandler {

  import KafkaProducer._

  private[this] var producer: Producer[String, Array[Byte]] = _


  override def receive: Receive = {
    case msg: Batch =>
      if (produceToKafka(msg))
        sender() ! Write(ByteString.fromArray(serialize[Ack](new Ack(msg.parcelId)).get))
    case x: Any =>
      logger.warn("Unknown message {}", x)
      unhandled(x)
  }

  def produceToKafka(batch: Batch): Boolean = {
    try {
      /**
       * We select the key as the combination of channel and clientUUID and content business type.
       */
      val data = batch.events.map(event => KeyedMessage[String, Array[Byte]](topic, event.channel + event.clientUUID + event.content.`type`, null, serialize[Event](event).get))
      producer.send(data)
      messagesPersistedToKafka.addAndGet(batch.getEvents.size())
      true
    }
    catch {
      case t: Throwable =>
        logger.error("Failed to process batch message with UUID {}, cause {}", batch.parcelId, t.getMessage)
        false
    }
  }

  @throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    super.preStart()
    producer = createProducer(brokers)
    logger.info("KafkaProducer Started...")
  }

  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    super.postStop()
    producer.close
  }
}
