package com.pollfish.event.aggregator.server

import kafka.producer.Partitioner
import kafka.utils.VerifiableProperties

/**
 * Partitions based on the length of the key. The choice of length can be not effective leading to a small subset of partitions actually used.
 * Based on the data that can be adjusted.
 */
class EventPartitioner extends Partitioner {

  def this(verifiableProperties: VerifiableProperties) = {
    this
  }

  override def partition(key: Any, numPartitions: Int): Int = {
    key match {
      case x: String => x.length % numPartitions
      case _: Any => 0
    }
  }
}