namespace java com.pollfish.event.aggregator.model

# A i64 type disguised as serial for event identification in a per-client or global scope.
typedef i64 serial

# A time structure that includes a utc time and a timezone identifier.
struct Time {
  1: i64 utcTime,
  2: string timeZone
}

# A structure that contains origin information such as ip or geo-location of the sender.
struct Origin {
  1: optional string ip,
  2: optional string location
}

# The content of the message. SchemaId is an optional field that can be used for content validation from well defined schema definitions.
# The type field denotes the business type of the message.
struct Content {
  1: string content,
  2: string type,
  3: optional string schemaId
}

# The event message structure. Ordinal field can be used in a per-client or global scope to aid in ordering, reconciliation and acknowledgement purposes.
# Channel field denotes the channel from where the message was produced.
# ClientUUID field denoted the identifier of the producer of the message.
struct Event {
  1: serial ordinal,
  2: i16 version = 1,
  3: string channel = "default",
  4: Time time,
  5: Content content,
  6: string clientUUID,
  7: optional Origin origin
}

# A batch structure that contains a list of events. This type hasn't any business semantics, it is only used for transport efficiency between client and server
# and to aid the acknowledgement mechanism by providing a parcelId to correlate the server acknowledgement.
struct Batch {
  1: list<Event> events
  2: string parcelId
}

# A acknowledgement message from server to client to signal that the batch with parcelId X has been successfully processed by the server.
# This type hasn't any business semantics.
struct Ack {
  1: string parcelId
}
