### Project Summary

The purpose of the project is the implementation of an aggregation system for logging events. A client module sends randomly generated events to the server
and pushes them to Kafka. A consumer module consumes from kafka and writes them to a Cassandra database.

### Event-Aggregator User Instructions

1. Clone code: git clone https://ckaratza@bitbucket.org/ckaratza/event-aggregator.git
2. After checking out use maven to build deployable artifact (runs in the parent pom).
After running *mvn -P production clean install* , 2 tar files will be created under ../client/target with the name thrift-client.tar and one under ../server/client with the name thrift-server.tar
3. Untar thrift-client.tar, thrift-server.tar (tar -xvf <filename>.tar). After extracting each tar, 4 folders will be created:bin, config, lib, logs
4. 
a)  bin folder contains 1 shell script : run.sh which starts the application
b) config folder contains all the system configuration (logback, configuration)
c) lib folder contains all the jars that form the classpath for the executable shell scripts
d) logs folder contains all the logging of the application

5. Before starting the application you need to install Kafka.

### Install Kafka
Download kafka from https://www.apache.org/dyn/closer.cgi?path=/kafka/0.8.2.0/kafka_2.11-0.8.2.0.tgz

### Install Thrift
> sudo apt-get install thrift-compiler

Please be careful that depending on which version your OS installs (check it with > thrift –version) you may have to adjust the parent pom.xml 

<apache.thrift.version>YOUR_VERSION</apache.thrift.version>
and
<apache.thrift.bin>/usr/bin/thrift or YOUR thrift bin path</apache.thrift.bin>
and 
 run mvn -P production clean install

### Start zookeeper
> bin/zookeeper-server-start.sh config/zookeeper.properties
start kafka with default configuration
> bin/kafka-server-start.sh config/server.properties

### Create events topic
> bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 3 --topic events

### Start Application Modules 
> thrift-server/bin/./run.sh
and then thrift-client	
> thrift-client/bin/./run.sh


### A few information on the project structure
The application uses a standard maven multi-module structure comprising of 5 projects

1. /event-aggregator/client (aka client)
client project contains the code of the thrift-client. It consists of a streaming tcp-client that uses thrift's serialization mechanism to send-receive messages and a simplistic event generator that sends messages to the server.
2. /event-aggregator/server (aka server)
server project contains the code of the thrift-server. It consists of a tcp- server that uses thrift's serialization mechanism to send-receive messages and subsequently save them to kafka.
3. /event-aggregator/consumer (aka consumer)
consumer project → TBD
4. /event-aggregator/common (aka common)
common project contains some common code shared from other modules.
5. /event-aggregator/model (aka model)
model project contains the thrift model.

*All source code, thrift file, configuration files contain comments for every class, method, e.t.c. that would possibly need explanation*